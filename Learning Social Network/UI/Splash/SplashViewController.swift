//
//  SplashViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/11/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
      self.dismiss(animated: false, completion: nil)
      //      let loginVC = LoginViewController(nibName: "LoginViewController", bundle: nil)
      //      self.navigationController?.pushViewController(loginVC, animated: true)
      let loginVC = LoginViewController(nibName: "LoginViewController", bundle: nil)
      self.navigationController?.pushViewController(loginVC, animated: true)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    navigationController?.setNavigationBarHidden(false, animated: false)
  }
  
}
