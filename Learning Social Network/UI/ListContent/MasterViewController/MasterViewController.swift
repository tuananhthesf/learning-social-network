//
//  MasterViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/19/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {
  
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var btnBack: UIButton!
  
  @IBAction func backOnClick(_ sender: UIButton) {
    self.navigationController?.setNavigationBarHidden(false, animated: false)
    self.navigationController?.navigationBar.isHidden = false
    self.navigationController?.popViewController(animated: true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //self.navigationController?.navigationBar.isHidden = true
    initTabView()
    
    btnBack.layer.cornerRadius = 8
    btnBack.layer.borderWidth = 1
    btnBack.layer.borderColor = UIColor.white.cgColor
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
  func initTabView() {
    let tabContent = TabContentViewController(nibName: "TabContentViewController", bundle: nil)
    self.view.addSubview(tabContent.view)
    self.addChild(tabContent)
    self.view.sendSubviewToBack(tabContent.view)
    
    tabContent.view.translatesAutoresizingMaskIntoConstraints = false
    tabContent.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    tabContent.view.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
    tabContent.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    tabContent.view.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    tabContent.view.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
  }
  
}
