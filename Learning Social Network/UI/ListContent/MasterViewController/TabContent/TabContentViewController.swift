//
//  TabContentViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/19/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import Floaty

class TabContentViewController: UIViewController {
  
  var tabBarCtl: UITabBarController!
  var tokenSession: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    createTabbarController()

  }
  
  func createTabbarController(){
    tabBarCtl = UITabBarController()
    
    let contentVC = ContentViewController(nibName: "ContentViewController", bundle: nil)
    //    contentVC.tokenSession = self.tokenSession
    let naviContent = UINavigationController(rootViewController: contentVC)
    setTabBarContent(viewcontroller: contentVC, title: "Content", imageName: "course_black", selectedImageName: "course_blue")
    //
    let memberVC = MemberViewController(nibName: "MemberViewController", bundle: nil)
    //    profileVC.tokenSession = self.tokenSession
    let naviMember = UINavigationController(rootViewController: memberVC)
    setTabBarContent(viewcontroller: memberVC, title: "Member", imageName: "account_icon", selectedImageName: "account_icon")
    
    let attendanceVC = AttendanceViewController(nibName: "AttendanceViewController", bundle: nil)
    let naviAttendance = UINavigationController(rootViewController: attendanceVC)
    setTabBarContent(viewcontroller: attendanceVC, title: "Attendance", imageName: "help_black", selectedImageName: "help_blue")
    
    let calendarVC = CalenderViewController(nibName: "CalenderViewController", bundle: nil)
    let naviCalendar = UINavigationController(rootViewController: calendarVC)
    setTabBarContent(viewcontroller: calendarVC, title: "Calendar", imageName: "contact_black", selectedImageName: "contact_blue")
    
    let pendingVC = PendingItemsViewController(nibName: "PendingItemsViewController", bundle: nil)
    let naviPending = UINavigationController(rootViewController: pendingVC)
    setTabBarContent(viewcontroller: pendingVC, title: "Pending Items", imageName: "help_black", selectedImageName: "help_blue")
    
    tabBarCtl.viewControllers = [naviContent, naviMember, naviAttendance, naviCalendar, naviPending]
    
    self.view.addSubview(tabBarCtl.view)
  }
  
  func setTabBarContent(viewcontroller: UIViewController, title: String, imageName: String, selectedImageName: String){
    viewcontroller.navigationController?.setNavigationBarHidden(true, animated: false)
    viewcontroller.tabBarItem.title = title
    viewcontroller.tabBarItem.image = UIImage(named: imageName)
    viewcontroller.tabBarItem.selectedImage = UIImage(named: selectedImageName)
  }

  
}
