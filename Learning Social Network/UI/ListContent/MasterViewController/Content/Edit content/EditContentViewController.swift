//
//  EditContentViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/19/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class EditContentViewController: UIViewController {
  
  @IBOutlet weak var pickerView: UIView!
  @IBOutlet weak var btnDone: UIButton!
  
  @IBOutlet weak var tfTitle: UITextField!
  @IBOutlet weak var lblStartDate: UILabel!
  @IBOutlet weak var lblEndDate: UILabel!
  @IBOutlet weak var lblLevel: UILabel!
  @IBOutlet weak var tfDescript: UITextField!
  
  var isClosed = false
  var isEdit: Bool = false
  var listContentModel: ListContentModel?
  
  @IBAction func selectLevelClick(_ sender: UIButton) {
    if !pickerView.isHidden {
      pickerView.isHidden = true
      return
    }
    pickerView.isHidden = false
  }
  
  @IBAction func closePicker(_ sender: UIButton) {
    if !pickerView.isHidden {
      pickerView.isHidden = true
    }
  }
  
  @IBAction func cancelClick(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func doneClick(_ sender: UIButton) {
    if isEdit {
      
    }
    else {
      
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    pickerView.isHidden = true
    initDoneButton()
  }
  
  func initDoneButton() {
    if isEdit {
      tfTitle.text = listContentModel?.contentName ?? ""
      tfDescript.text = listContentModel?.contentDescript ?? ""
      
      lblEndDate.text = "\(listContentModel?.endDate ?? 1)"
      lblStartDate.text = "\(listContentModel?.startDate ?? 1)"
      lblLevel.text = listContentModel?.level ?? ""
      
      btnDone.setTitle("Save", for: .normal)
    }
  }
  
}
