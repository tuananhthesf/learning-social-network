//
//  Sub1ViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/19/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import Floaty

class ContentViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initFloating()
    
    tableView.delegate = self
    tableView.dataSource = self
    
    let nib = UINib(nibName: "CustomContentTableViewCell", bundle: Bundle.main)
    self.tableView.register(nib, forCellReuseIdentifier: "CustomContentTableViewCell")
  }
  
  func initFloating() {
    let floaty = Floaty()
    floaty.addItem(title: "Hello, World!")
    floaty.sticky = true
    self.view.addSubview(floaty)
    self.view.bringSubviewToFront(floaty)
  }
  
}

extension ContentViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CustomContentTableViewCell", for: indexPath) as! CustomContentTableViewCell
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let mContent = ListContentModel(userName: "Tuan Anh", picture: "Ngu", status: "Ok", startDate: 123123, endDate: 123123, level: "Professional", contentName: "Tuấn Anh Đẹp Trai", contentDescript: "Đẹp trai VCL")
    
    let detailVC = DetailViewController(nibName: "DetailViewController", bundle: nil)
    self.navigationController?.pushViewController(detailVC, animated: true)
  }
  
  
}
