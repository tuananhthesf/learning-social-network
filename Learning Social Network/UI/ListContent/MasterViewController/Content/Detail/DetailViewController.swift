//
//  DetailViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/21/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  @IBOutlet weak var imgContent: UIImageView!
  @IBOutlet weak var lbContentTitle: UILabel!
  @IBOutlet weak var lbStartDate: UILabel!
  @IBOutlet weak var lbEndDate: UILabel!
  @IBOutlet weak var lbAuthorName: UILabel!
  @IBOutlet weak var descriptionTextView: UITextView!
  @IBOutlet weak var takeAttendaceButton: UIButton!
  @IBOutlet weak var btnEdit: UIButton!
  
  var listContentModel: ListContentModel?
  
  @IBAction func editOnClick(_ sender: Any) {
    let editVC = EditContentViewController(nibName: "EditContentViewController", bundle: nil)
    editVC.isEdit = true
    self.navigationController?.pushViewController(editVC, animated: true)
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    takeAttendaceButton.layer.cornerRadius = 8
    takeAttendaceButton.layer.borderWidth = 1
    takeAttendaceButton.layer.borderColor = UIColor.white.cgColor
    
    borderBTN()
  }
  
  func borderBTN() {
    btnEdit.layer.cornerRadius = (btnEdit.frame.size.width / 2)
    btnEdit.layer.masksToBounds = true
    btnEdit.layer.borderWidth = 1
    btnEdit.layer.borderColor = UIColor (red:255 ,  green: 255, blue: 255 , alpha: 1).cgColor
  }
  
}
