//
//  CustomContentTableViewCell.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/19/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class CustomContentTableViewCell: UITableViewCell {
  
  @IBOutlet weak var imgAvatar: UIImageView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
      self.imgAvatar.layer.cornerRadius = (self.imgAvatar.frame.size.width / 2);
      self.imgAvatar.layer.masksToBounds = true;
      self.imgAvatar.layer.borderWidth = 2;
      self.imgAvatar.layer.borderColor = UIColor (red:255 ,  green: 255, blue: 255 , alpha: 1).cgColor;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
