//
//  ListContentViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/18/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class ListContentViewController: UIViewController {
  
  var tabBarCtl: UITabBarController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func createTabbarController(){
    tabBarCtl = UITabBarController()
    
    let courseVC = CourseViewController(nibName: "CourseViewController", bundle: nil)
    let navi1 = UINavigationController(rootViewController: courseVC)
    setTabBarContent(viewcontroller: courseVC, title: "Course", imageName: "course_black", selectedImageName: "course_blue")
    
    tabBarCtl.viewControllers = [navi1, contactsVC, helpVC, faqVC, profileVC]
    
    self.view.addSubview(tabBarCtl.view)
  }
  
  func setTabBarContent(viewcontroller: UIViewController, title: String, imageName: String, selectedImageName: String){
    viewcontroller.tabBarItem.title = title
    viewcontroller.tabBarItem.image = UIImage(named: imageName)
    viewcontroller.tabBarItem.selectedImage = UIImage(named: selectedImageName)
  }
  
}
