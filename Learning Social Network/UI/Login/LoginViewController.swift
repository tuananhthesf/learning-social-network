//
//  LoginViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/11/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleSignIn
import Toast_Swift
import Floaty

class LoginViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
  
  @IBOutlet weak var loginButton: GIDSignInButton!
  
  var dataModel:[APIModel] = []
  
  @IBAction func signInGoogle(_ sender: GIDSignInButton!) {
    //GIDSignIn.sharedInstance().signIn()
    
        let mainVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
        self.navigationController?.pushViewController(mainVC, animated: true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    GIDSignIn.sharedInstance().uiDelegate = self
    GIDSignIn.sharedInstance().delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(true)
    navigationController?.setNavigationBarHidden(false, animated: false)
  }
  
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
            withError error: Error!) {
    
    if let error = error {
      print("\(error.localizedDescription)")
    } else {
      let accessToken = user.authentication.accessToken // Safe to send to the server
      print(accessToken ?? "---- Tuan Anh Dep Trai")
      
      //postToken(token: accessToken ?? "loi", name: user.profile.email)
      
      postToken(token: [
        "avatar":"\(String(user.authentication.accessToken) ?? "OK" )",
        "email":"\(String(user.profile.email) ?? "OK" )",
        "full_name":"\(String(user.profile.name) ?? "OK")",
        "password":"Tuấn Anh Đẹp Trai Vô Địch"
        ],name: user.profile.email)
      
      print("\(user.profile.email),\(user.authentication.idToken),\(user.authentication.accessToken),\(user.profile.name)")
      
    }
  }
  
  func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
            withError error: Error!) {
    
  }
  
  func postToken(token: [String:Any], name: String) {
    AF.request(Router.loginAccess(body: token)).responseJSON{ response in
      switch response.result {
      case .success(let value):
        print("--------------------")
        if let json = value as? [String: Any] {
          
          //print(json)
          
          var tokenSession = "token"

          //print(json["data"])
          
          if let data = json["data"] as? [String: Any] {
            tokenSession = (data["token"] as? String)!
            
            print("------ \(tokenSession)")
          }
          
          if let code = json["code"] as? Int {
            print(code)
            switch code {
            case 0:
              self.view.makeToast("Email của bạn không được phép đăng nhập vào hệ thống")
              GIDSignIn.sharedInstance().signOut()
            case 200:
              let homeVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
             // homeVC.tokenSession = tokenSession
              self.navigationController?.pushViewController(homeVC, animated: true)
              homeVC.view.makeToast("Xin chào: \(name)")
              
            default:
              print(code)
            }
          }
        }
      case .failure(_):
        self.view?.makeToast("Đăng nhập thất bại!")
      }
    }
    
  }
}
