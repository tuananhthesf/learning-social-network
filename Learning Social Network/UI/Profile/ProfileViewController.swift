//
//  ProfileViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/13/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import GoogleSignIn
import Alamofire
import Toast_Swift
import SwiftyJSON

class ProfileViewController: UIViewController {
  
  @IBOutlet weak var avaImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var classLabel: UILabel!
  @IBOutlet weak var teamLabel: UILabel!
  @IBOutlet weak var langLabel: UILabel!
  var tokenSession: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.avaImage.layer.cornerRadius = (self.avaImage.frame.size.width / 2)
    self.avaImage.layer.masksToBounds = true
    self.avaImage.layer.borderWidth = 2
    self.avaImage.layer.borderColor = UIColor (red:255 ,  green: 255, blue: 255 , alpha: 1).cgColor
    // Do any additional setup after loading the view.
    
    
  }
  
  @IBAction func logOut(_ sender: UIButton) {
    print(tokenSession ?? "")
    logoutWithServer(token: ["token":tokenSession])
  }
  
  
  func logoutWithServer(token: [String: Any]){
    AF.request(Router.logout(body: token)).responseJSON{ response in
      print(response.result)
      switch response.result {
        
      case .success(let value):
        
        print (value)
        if let json = value as? [String: Any]{
          print (json)
          if let code = json["code"] as? Int {
            
            switch code {
            case 0:

              self.view.makeToast("Somethings happened, please try again!")
              return
            case 1:
              GIDSignIn.sharedInstance().signOut()
              let appDelegate = UIApplication.shared.delegate as! AppDelegate
              //appDelegate.switchBack()
              if let loginVC = self.navigationController?.viewControllers[1] {
                self.navigationController?.popToViewController(loginVC, animated: true)
              }
              appDelegate.window?.rootViewController = appDelegate.navi
            default:
              print (code)
              return
            }
          }
        }
      case .failure(_):
        self.view.makeToast("Somethings happened, please try again!")
      }
    }
  }
}

