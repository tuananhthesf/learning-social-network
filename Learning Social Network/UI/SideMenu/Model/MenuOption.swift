//
//  MenuOption.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/19/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

enum MenuOption: Int, CustomStringConvertible {
  
  case Profile
  case Courses
  case Contacts
  case Help
  case FAQ
  case LOGOUT
  
  var description: String {
    switch self {
    case .Profile: return "Profile"
    case .Courses: return "Courses"
    case .Contacts: return "Contacts"
    case .Help: return "Help"
    case .FAQ: return "FAQ"
    case .LOGOUT: return "Logout"
    }
  }
  
  var image: UIImage {
    switch self {
    case .Profile: return UIImage(named: "account_icon") ?? UIImage()
    case .Courses: return UIImage(named: "courses-white") ?? UIImage()
    case .Contacts: return UIImage(named: "contacts-white") ?? UIImage()
    case .Help: return UIImage(named: "help-white") ?? UIImage()
    case .FAQ: return UIImage(named: "question_white") ?? UIImage()
    case .LOGOUT: return UIImage(named: "logout") ?? UIImage()
    }
  }
}
