//
//  ContainerViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/18/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//



import UIKit

class ContainerViewController: UIViewController {
  
  var menuController: UIViewController!
  var homeController: UIViewController!
  var isExpanded = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
    configureNavigationBar()
    
    // Do any additional setup after loading the view.
  }
  
  @objc func handleMenuToogle(){
    if !isExpanded {
      setupMenuController()
    }
    else {
      removeMenuController()
    }
    isExpanded = !isExpanded
    showSideMenu(shouldExpand: isExpanded)
    print("tapped toogle")
  }
  
  func configureNavigationBar(){
    navigationController?.navigationBar.barTintColor = .darkGray
    navigationController?.navigationBar.barStyle = .black

    navigationItem.title = "Home"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "course_black"), style: .plain, target: self, action: #selector(handleMenuToogle))
  }

  func setupMenuController(){
    if menuController == nil {
      menuController = SideMenuViewController()
      view.insertSubview(menuController.view, at: 0)
      addChild(menuController)
      menuController?.didMove(toParent: self)
      print ("asdasdsad")
    }
  }
  
  func removeMenuController(){
    menuController!.willMove(toParent: nil)
    menuController!.view.removeFromSuperview()
    menuController!.removeFromParent()
  }
  
  func showSideMenu(shouldExpand: Bool){
    if shouldExpand {
      UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {self.view.frame.origin.x = self.view.frame.width - 80 }, completion: nil)
    } else {
      //      removeMenuController()
      UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {self.view.frame.origin.x = 0 }, completion: nil)
      
    }
  }
  
}


