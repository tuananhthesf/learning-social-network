//
//  SideMenuViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/18/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

  
  @IBOutlet weak var imgAvatar: UIImageView!
  
  @IBOutlet weak var tableView: UITableView!
  var tokenSession: String?
  
  override func viewDidLoad() {
        super.viewDidLoad()
    
    self.borderImage()
    self.view.backgroundColor = #colorLiteral(red: 0.2211332023, green: 0.4911122918, blue: 0.6980797648, alpha: 1)
    
    tableView.backgroundColor = #colorLiteral(red: 0.2211332023, green: 0.4911122918, blue: 0.6980797648, alpha: 1)
    tableView.separatorStyle = .none
    tableView.rowHeight = 60
    tableView.isScrollEnabled = false
    tableView.delegate = self
    tableView.dataSource = self
    
    let nib = UINib(nibName: "MenuTableViewCell", bundle: Bundle.main)
    self.tableView.register(nib, forCellReuseIdentifier: "MenuTableViewCell")
    
    }
  
  func borderImage() {
    imgAvatar.layer.cornerRadius = (imgAvatar.frame.size.width / 2)
    imgAvatar.layer.masksToBounds = true
    imgAvatar.layer.borderWidth = 1
    imgAvatar.layer.borderColor = UIColor.black.cgColor
  }

}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 6
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
    let menuOption = MenuOption(rawValue: indexPath.row)
    cell.descriptionLabel.text = menuOption?.description
    cell.iconImageView.image = menuOption?.image
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if indexPath.row == 5 {
      cell.contentView.backgroundColor = #colorLiteral(red: 0.9382099509, green: 0.2959643602, blue: 0.4225458205, alpha: 1)
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let menuOption = MenuOption(rawValue: indexPath.row)
    switch menuOption {
    case .Profile?:
      let profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
      profileVC.tokenSession = self.tokenSession
      navigationController?.pushViewController(profileVC, animated: true)
      print("Profile")
    case .Courses?:
      let coursesVC = CourseViewController(nibName: "CourseViewController", bundle: nil)
      coursesVC.tokenSession = self.tokenSession
      navigationController?.pushViewController(coursesVC, animated: true)
      print("Show Inbox")
    case .Contacts?:
      let contactsVC = ContactsViewController(nibName: "ContactsViewController", bundle: nil)
      navigationController?.pushViewController(contactsVC, animated: true)
      print("Show Notifications")
    case .Help?:
      let helpVC = HelpViewController(nibName: "HelpViewController", bundle: nil)
      navigationController?.pushViewController(helpVC, animated: true)
      print("Show Help")
    case .some(.FAQ):
      let faqVC = FAQViewController(nibName: "FAQViewController", bundle: nil)
      navigationController?.pushViewController(faqVC, animated: true)
      print("Show FAQ")
    case .none:
      return
    case .some(.LOGOUT):
      print("OK")
    }
  }
  
  
}
