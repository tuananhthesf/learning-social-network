//
//  MenuTableViewCell.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/18/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

  @IBOutlet weak var iconImageView: UIImageView!
  
  @IBOutlet weak var descriptionLabel: UILabel!
  override func awakeFromNib() {
        super.awakeFromNib()
     backgroundColor = #colorLiteral(red: 0.2211332023, green: 0.4911122918, blue: 0.6980797648, alpha: 1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
