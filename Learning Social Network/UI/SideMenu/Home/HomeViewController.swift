//
//  HomeViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/18/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  
  var menuController: SideMenuViewController!
  var centerController: CenterViewController!
  var isExpanded = false
  var tokenSession: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.tintColor = UIColor.white
    self.view.backgroundColor = #colorLiteral(red: 0.2211332023, green: 0.4911122918, blue: 0.6980797648, alpha: 1)
    centerController = CenterViewController()
    view.addSubview(centerController.view)
    addChild(centerController)
    centerController.didMove(toParent: self)
    configureNavigationBar()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    if isExpanded {
      isExpanded = false
      removeMenuController()
      UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {self.centerController.view.frame.origin.x = 0 }, completion: nil)
    }
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .default
  }
  
  override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
    return .slide
  }
//  
//  override var prefersStatusBarHidden: Bool {
//    return isExpanded
//  }
//  
  @objc func handleMenuToogle(){
    if !isExpanded {
      setupMenuController()
    }
    isExpanded = !isExpanded
    showSideMenu(shouldExpand: isExpanded)
    print("tapped toogle")
  }
  
  func configureNavigationBar(){
    navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.2211332023, green: 0.4911122918, blue: 0.6980797648, alpha: 1)
    navigationController?.navigationBar.barStyle = .blackOpaque
    
    navigationItem.title = "Home"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_white_3x"), style: .plain, target: self, action: #selector(handleMenuToogle))
  }
  
  func setupMenuController(){
    if menuController == nil {
      menuController = SideMenuViewController()
      menuController.tokenSession = self.tokenSession
    }
  }
  
  
  
  func removeMenuController(){
    menuController!.willMove(toParent: nil)
    menuController!.view.removeFromSuperview()
    menuController!.removeFromParent()
  }
  
  func showSideMenu(shouldExpand: Bool){
    
    if shouldExpand {
      UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80 }, completion: nil)
      view.insertSubview(menuController.view, at: 0)
      addChild(menuController)
      menuController?.didMove(toParent: self)
      print ("asdasdsad")
    } else {
      removeMenuController()
      UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {self.centerController.view.frame.origin.x = 0 }, completion: nil)
      
    }
  }
  
  
  
}

