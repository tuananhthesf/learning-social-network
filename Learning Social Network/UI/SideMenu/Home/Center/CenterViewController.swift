//
//  CenterViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/18/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class CenterViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
        super.viewDidLoad()
         view.backgroundColor = .blue
    
    tableView.delegate = self
    tableView.dataSource = self
    
    let nib = UINib(nibName: "CourseTableViewCell", bundle: Bundle.main)
    self.tableView.register(nib, forCellReuseIdentifier: "CourseTableViewCell")
    }

}

extension CenterViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 4
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CourseTableViewCell", for: indexPath) as! CourseTableViewCell
    
    return cell
  }
  
  
}
