//
//  CourseTableViewCell.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/13/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import Kingfisher

class CourseTableViewCell: UITableViewCell {
  
  @IBOutlet weak var imgAvatar: UIImageView!
  
  @IBOutlet weak var courseName: UILabel!
  @IBOutlet weak var members: UILabel!
  @IBOutlet weak var courseDescript: UILabel!
  
  @IBOutlet weak var bgView: UIView!
  
  
  var mCourse: CourseModel? {
    didSet {
      self.updateData()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func updateData() {
    self.courseName.text = self.mCourse?.courseName ?? ""
    members.text = "20 members"
    courseDescript.text = "akjsdfkljas ll;kasjd fkjasd f;klasd flkasjd flaksdj falksjd flkajsd fkajsd flkajs dflaksj flasd fkaljsdf askdjf alskdfj laksdf lasdkj flkasdf alsdf laksdjf al;skdfj alskdj falskdfj asl;kdf"
    //mCourse?.description ?? ""
    
    let imgURL = URL(string: mCourse?.picture ?? "")
    imgAvatar.kf.setImage(with: imgURL)
  }
  
}
