//
//  CourseViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/13/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CourseViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var tokenSession: String?
  var mCourse: CourseModel?
  
  var courseData:[CourseModel] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.visibleViewController?.title = "Courses"
    
    tableView.delegate = self
    tableView.dataSource = self
    
    let nib = UINib(nibName: "CourseTableViewCell", bundle: Bundle.main)
    self.tableView.register(nib, forCellReuseIdentifier: "CourseTableViewCell")
    
    getCourseList(token_session: self.tokenSession ?? "token")
  }
  
  func getCourseList(token_session: String) {
    AF.request(Router.listCourse(headers: ["token_session": token_session])).responseJSON{ response in
      switch response.result {
      case .success(let value):
        print("-------------Course List----------------")
        
        if let json = value as? [String: Any] {
          let jsons = JSON(json["data"]!)
          let course = jsons["courses"]
          
          let listCourse = course.arrayValue
          
          for mCourse in listCourse {
            let mData = CourseModel(withJSON: mCourse)
            
            self.courseData.append(mData)
          }
        }
        self.tableView.reloadData()
      case .failure(_):
        self.view?.makeToast("Đăng nhập thất bại!")
      }
    }
  }
}

extension CourseViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //return courseData.count
    return 10
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CourseTableViewCell", for: indexPath) as! CourseTableViewCell
    //cell.mCourse = courseData[indexPath.row]
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("You tapped cell number \(indexPath.row)")
    let masterVC = MasterViewController(nibName: "MasterViewController", bundle: nil)
    self.navigationController?.pushViewController(masterVC, animated: true)
  }
  
}
