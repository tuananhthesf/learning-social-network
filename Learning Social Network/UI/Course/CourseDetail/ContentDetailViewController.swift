//
//  ContentDetailViewController.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/14/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class ContentDetailViewController: UIViewController {
  
  @IBOutlet weak var scrollView: UIScrollView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func centerScroll() {
    let centerOffsetX = (scrollView.contentSize.width - scrollView.frame.size.width) / 2
    let centerOffsetY = (scrollView.contentSize.height - scrollView.frame.size.height) / 2
    let centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
    scrollView.setContentOffset(centerPoint, animated: true)
  }
  
}
