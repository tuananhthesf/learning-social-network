//
//  MainViewController.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/12/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
  
  var tabBarCtl: UITabBarController!
  var tokenSession: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    createTabbarController()
  }
  
  func createTabbarController(){
    tabBarCtl = UITabBarController()
    
    let courseVC = CourseViewController(nibName: "CourseViewController", bundle: nil)
    courseVC.tokenSession = self.tokenSession
    let navi1 = UINavigationController(rootViewController: courseVC)
    setTabBarContent(viewcontroller: courseVC, title: "Course", imageName: "course_black", selectedImageName: "course_blue")
    
    let profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
    profileVC.tokenSession = self.tokenSession
    setTabBarContent(viewcontroller: profileVC, title: "Profile", imageName: "account_icon", selectedImageName: "account_icon")
    
    let helpVC = HelpViewController(nibName: "HelpViewController", bundle: nil)
    setTabBarContent(viewcontroller: helpVC, title: "Help", imageName: "help_black", selectedImageName: "help_blue")
    
    let contactsVC = ContactsViewController(nibName: "ContactsViewController", bundle: nil)
    setTabBarContent(viewcontroller: contactsVC, title: "Contact", imageName: "contact_black", selectedImageName: "contact_blue")
    
    let faqVC = FAQViewController(nibName: "FAQViewController", bundle: nil)
    setTabBarContent(viewcontroller: faqVC, title: "FAQ", imageName: "help_black", selectedImageName: "help_blue")
    
    tabBarCtl.viewControllers = [navi1, contactsVC, helpVC, faqVC, profileVC]
    
    self.view.addSubview(tabBarCtl.view)
  }
  
  func setTabBarContent(viewcontroller: UIViewController, title: String, imageName: String, selectedImageName: String){
    viewcontroller.tabBarItem.title = title
    viewcontroller.tabBarItem.image = UIImage(named: imageName)
    viewcontroller.tabBarItem.selectedImage = UIImage(named: selectedImageName)
  }
  
}

