//
//  AppDelegate.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/11/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  {
  
  var window: UIWindow?
  var navi: UINavigationController?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    let splashViewController = SplashViewController()
    navi = UINavigationController(rootViewController: splashViewController)
    window!.rootViewController = navi
    window!.makeKeyAndVisible()
    GIDSignIn.sharedInstance().clientID = "1087844000472-ba7e9mdr5ao7qdokae51rjtd224719dm.apps.googleusercontent.com"
    return true
  }
  
//  func switchBack() {
//    // switch back to view controller 1
//    let splashViewController = SplashViewController()
//    let navi = UINavigationController(rootViewController: splashViewController)
//    self.window?.rootViewController = navi
//  }
  

  
  func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    return GIDSignIn.sharedInstance().handle(url as URL?,
                                             sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                             annotation: options[UIApplication.OpenURLOptionsKey.annotation])
  }
  
  func switchNavi() {
    let loginViewController = LoginViewController()
    let navi = UINavigationController(rootViewController: loginViewController)
    
    self.window?.rootViewController = navi
  }

}

