//
//  testAlam.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/12/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum Router: URLRequestConvertible {
  
  case loginAccess(body: Parameters)
  case logout(body: Parameters)
  case listCourse(headers: [String:String])
  
  case reqRes(body: Parameters)
  
  var result: (path: String, method: HTTPMethod, body: Parameters?, params: Parameters?, headers: Parameters?) {
    switch self {
      //    case .loginAccess(let body):
      //      return ("/v1/sessions/", .post, body, nil, nil)
      
    case .loginAccess(let body):
      return ("/api/login", .post, body, nil, nil)
      
      //    case .logout(let params):
      //      return ("/v1/sessions/\(params)", .delete, nil, nil, nil)
      
    case .logout(let body):
      return ("/api/logout", .post, body, nil, nil)
      
      
    case .reqRes(let body):
      return ("/api/login/", .post, body, nil, nil)
    case .listCourse(let headers):
      return ("/v1/courses", .get, nil, nil, headers)
    }
  }
  
  func asURLRequest() throws -> URLRequest {
    let url: URL =  try Const.BASE_URL.asURL()
    let result = self.result
    var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
    urlRequest.httpMethod = result.method.rawValue
    print(urlRequest)
    
    urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
    
    //body data
    if let headers = result.headers {
      urlRequest.allHTTPHeaderFields = (headers as! [String : String])
    }
    if let body = result.body {
      let bodyData = try JSONSerialization.data(withJSONObject: body, options: [])
      urlRequest.httpBody = bodyData
    }
    if let params = result.params {
      let urlEncode = try URLEncoding.default.encode(urlRequest, with: params)
      print(urlEncode)
      return urlEncode
    }
    return urlRequest
  }
}
