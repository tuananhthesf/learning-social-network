//
//  APIModel.swift
//  Learning Social Network
//
//  Created by Hieu Nguyen on 2/12/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import Foundation
import SwiftyJSON;

class APIModel {
  var result: Bool
  var tokenSession: String
  var email: String
  var name: String
  var picture: String
  
  init(withJSON data: JSON) {
    self.result = data["result"].boolValue
    self.tokenSession = data["tokenSession"].stringValue
    self.email = data["email"].stringValue
    self.name = data["name"].stringValue
    self.picture = data["picture"].stringValue
  }
  
}
