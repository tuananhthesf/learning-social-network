//
//  CourseModel.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/20/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import Foundation
import SwiftyJSON

class CourseModel {
  var courseID: String?
  var courseName: String?
  var picture: String?
  var description: String?
  
  init(withJSON data: JSON) {
    self.courseID = data["courseId"].stringValue
    self.courseName = data["courseName"].stringValue
    self.picture = data["picture"].stringValue
    self.description = data["description"].stringValue
  }

}
