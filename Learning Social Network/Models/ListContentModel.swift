//
//  File.swift
//  Learning Social Network
//
//  Created by Nguyen Tuan Anh on 2/20/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import Foundation

class ListContentModel {
  
  var userName: String
  var picture: String
  var status: String
  var startDate: Double
  var endDate: Double
  var level: String
  var contentName: String
  var contentDescript: String
  
  init(userName: String, picture: String, status: String, startDate: Double, endDate: Double, level: String, contentName: String, contentDescript: String) {
    self.userName = userName
    self.picture = picture
    self.status = status
    self.startDate = startDate
    self.endDate = endDate
    self.level = level
    self.contentName = contentName
    self.contentDescript = contentDescript
  }
}
